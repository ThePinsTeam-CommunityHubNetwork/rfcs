# RFCs for The Pins Team

The home of The Pins Team'crs RFCs for improving the organization and its projects. Changes to the handbook should be filed straight to our handbook repo instead (https://gitlab.com/MadeByThePinsHub/handbook/issues/new).